import axios from 'axios';

const axiosInterceptor = axios.create({
	baseURL: 'http://localhost:3080',
	headers: {
		authorization: 'mysecrettoken',
	},
});

export default axiosInterceptor;
