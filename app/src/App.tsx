import { useState, useEffect } from 'react';
import axiosInterceptor from './interceptors/axiosInterceptor';
import './App.css';

function App() {
  interface IEpochTime {
    epoch: number;
  }

  const [serverTime, setServerTime] = useState<IEpochTime>();
  const [loadingServerTime, setLoadingServerTime] = useState<boolean>(true);

  const [serverMetrics, setServerMetrics] = useState<string>('');
  const [loadingServerMetrics, setLoadingServerMetrics] =
    useState<boolean>(true);

  const [differenceTime, setDifferenceTime] = useState<string>('00:00:00');

  useEffect(() => {
    const fetchTime = async () => {
      setLoadingServerTime(true);
      await axiosInterceptor
        .get<IEpochTime>('/time')
        .then((response) => {
          setServerTime(response.data);
          setLoadingServerTime(false);
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.log(error); //Error handling not implemented therefore ignore console log
        });
    };

    const fetchMetrics = async () => {
      setLoadingServerMetrics(true);
      await axiosInterceptor
        .get<string>('/metrics')
        .then((response) => {
          setServerMetrics(response.data);
          setLoadingServerMetrics(false);
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.log(error); //Error handling not implemented therefore ignore console log
        });
    };

    const differenceCalculator = () => {
      const clientTime = Date.now();
      const clientTimeEpochSeconds = Math.round(clientTime / 1000);

      const difference =
        clientTimeEpochSeconds - (serverTime?.epoch || 0);
      const differenceString = new Date(1000 * difference)
        .toISOString()
        .slice(11, 19);

      setDifferenceTime(differenceString);
    };

    fetchTime();
    fetchMetrics();
    differenceCalculator();

    const interval = setInterval(() => {
      fetchTime();
      fetchMetrics();
    }, 30000);

    const differenceInterval = setInterval(() => {
      differenceCalculator();
    }, 900);

    return () => {
      clearInterval(interval);
      clearInterval(differenceInterval);
    };
  }, [serverTime?.epoch]);

  return (
    <div className={'splitScreen'}>
      <div className={'leftPane'}>
        {loadingServerTime ? (
          <p>Loading..</p>
        ) : (
          <>
            <p>{serverTime?.epoch}</p>
            <p>{differenceTime}</p>
          </>
        )}
      </div>
      <div className={`rightPane ${loadingServerMetrics && 'loading'}`}>
        {loadingServerMetrics ? (
          <p>Loading..</p>
        ) : (
          <p>{serverMetrics}</p>
        )}
      </div>
    </div>
  );
}

export default App;
