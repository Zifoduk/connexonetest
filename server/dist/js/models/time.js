"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getEpochTime = () => {
    const currentTime = Date.now();
    const epochSeconds = Math.round(currentTime / 1000);
    const time = { epoch: epochSeconds };
    return time;
};
exports.default = getEpochTime;
