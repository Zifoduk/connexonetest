"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const headerAuthorization = (req, res, next) => {
    if (!req.headers.authorization ||
        req.headers.authorization !== process.env.SECRET_TOKEN) {
        return res.status(403).json({ error: 'Not Authorized!' });
    }
    next();
};
exports.default = headerAuthorization;
