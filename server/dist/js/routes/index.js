"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const time_1 = require("../controllers/time");
const router = (0, express_1.Router)();
router.get('/time', time_1.getTime);
exports.default = router;
