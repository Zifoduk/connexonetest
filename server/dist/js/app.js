"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_prometheus_middleware_1 = __importDefault(require("express-prometheus-middleware"));
const cors_1 = __importDefault(require("cors"));
const routes_1 = __importDefault(require("./routes"));
const headerAuthorization_1 = __importDefault(require("./middleware/headerAuthorization"));
require("dotenv/config");
const app = (0, express_1.default)();
const PORT = process.env.PORT || 3080;
app.use((0, cors_1.default)());
app.use(headerAuthorization_1.default);
app.use(routes_1.default);
app.use((0, express_prometheus_middleware_1.default)({
    metricsPath: '/metrics',
    collectDefaultMetrics: true,
    requestDurationBuckets: [0.1, 0.5, 1, 1.5],
    requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
    responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
}));
app.all('*', (req, res) => {
    res.status(404);
    if (req.accepts('html')) {
        res.json({ message: '404 Not Found' });
    }
    else if (req.accepts('json')) {
        res.json({ message: '404 Not Found' });
    }
    else {
        res.type('txt').send('404 Not Found');
    }
});
// eslint-disable-next-line no-console
app.listen(PORT, () => console.log(`Server running on port ${PORT}`)); //eslint no console disabled because need output to know when server is running
