"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
describe('Test /health', () => {
    describe('Health check on /sync', () => {
        it('health should be okay', () => {
            const actualResult = healthCheckSync();
            (0, chai_1.expect)(actualResult).to.equal('OK');
        });
    });
});
