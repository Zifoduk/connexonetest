import { Request, Response, NextFunction } from 'express';
import 'dotenv/config';

const headerAuthorization = (
	req: Request,
	res: Response,
	next: NextFunction
) => {
	if (
		!req.headers.authorization ||
		req.headers.authorization !== process.env.SECRET_TOKEN
	) {
		return res.status(403).json({ error: 'Not Authorized!' });
	}
	next();
};

export default headerAuthorization;
