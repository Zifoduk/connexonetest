import { Response, Request } from 'express';
import { ITime } from '../../types/time';
import Time from '../../models/time';

const getTime = async (req: Request, res: Response): Promise<void> => {
	const time: ITime = Time();
	res.status(200).json(time);
};

export { getTime };
