import express, { Express } from 'express';
import promMid from 'express-prometheus-middleware';
import cors from 'cors';
import routes from './routes';
import headerAuthorization from './middleware/headerAuthorization';
import 'dotenv/config';

const app: Express = express();
const PORT: string | number = process.env.PORT || 3080;

app.use(cors());
app.use(headerAuthorization);
app.use(routes);
app.use(
	promMid({
		metricsPath: '/metrics',
		collectDefaultMetrics: true,
		requestDurationBuckets: [0.1, 0.5, 1, 1.5],
		requestLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
		responseLengthBuckets: [512, 1024, 5120, 10240, 51200, 102400],
	})
);

app.all('*', (req, res) => {
	res.status(404);
	if (req.accepts('html')) {
		res.json({ message: '404 Not Found' });
	} else if (req.accepts('json')) {
		res.json({ message: '404 Not Found' });
	} else {
		res.type('txt').send('404 Not Found');
	}
});

// eslint-disable-next-line no-console
app.listen(PORT, () => console.log(`Server running on port ${PORT}`)); //eslint no console disabled because need output to know when server is running
