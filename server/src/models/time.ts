import { ITime } from '../types/time';

const getEpochTime = () => {
	const currentTime: number = Date.now();
	const epochSeconds: number = Math.round(currentTime / 1000);
	const time: ITime = { epoch: epochSeconds };
	return time;
};

export default getEpochTime;
