import { Router } from 'express';
import { getTime } from '../controllers/time';

const router: Router = Router();

router.get('/time', getTime);

export default router;
